#!/usr/bin/python

# written by andrewt@cse.unsw.edu.au September 2015
# as a starting point for COMP2041/9041 assignment 2
# http://cgi.cse.unsw.edu.au/~cs2041/assignments/bitter/

import cgi, cgitb, glob, os

def main():
    print page_header()
    cgitb.enable()

    dataset_size = "medium" 
    users_dir = "dataset-%s/users"% dataset_size

    parameters = cgi.FieldStorage()
    print Login_page(parameters, users_dir)
    print page_trailer(parameters)


#
# Show unformatted details for user "n".
# Increment parameter n and store it as a hidden variable
#
def Login_page(parameters, users_dir):
    #user_name = form.getvalue('user_name')
    #password = form.getvalue('password')
    #n = int(parameters.getvalue('n', 0))
    #users = sorted(glob.glob(os.path.join(users_dir, "*")))
    #user_to_show  = users[n % len(users)]
    #details_filename = os.path.join(user_to_show, "details.txt")
    #with open(details_filename) as f:
    #    details = f.read()
    #for lines in details:   
    return """

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Flatfy Free Flat and Responsive HTML5 Template ">
    <meta name="author" content="">

    <title>Seahorse Ink.</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
 
    <!-- Custom Google Web Font -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Arvo:400,700' rel='stylesheet' type='text/css'>
    
    <!-- Custom CSS-->
    <link href="css/general.css" rel="stylesheet">
    

    <link href="css/simple-sidebar.css" rel="stylesheet">
    <link href="css/jath-sidebar.css" rel="stylesheet">


     <!-- Owl-Carousel -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    
    <!-- Magnific Popup core CSS file -->
    <link rel="stylesheet" href="css/magnific-popup.css"> 
    
    <script src="js/modernizr-2.6.2.min.js"></script>  <!-- Modernizr /-->
</head>

<body id="home">
    <div id="wapper">
        <div id="sidebar-wapper">
            <ul class="sidebar-navi">
                <li class="sidebar-brand">
                    <a href="#">
                        <span class="glyphicon glyphicon-tint" aria-hidden="true"></span>
                    </a>
                </li>
                <br>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>                         
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </li>
                <li>
                    <a href="#menu-open" id="menu-open">
                        <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div id="wrapper">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="#">
                        &nbsp;&nbsp;&nbsp;Seahorse Ink. 
                        <span class="glyphicon glyphicon-tint" aria-hidden="true"></span>
                    </a>
                </li>
                <br>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                        &nbsp;&nbsp;Home
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>                         
                        &nbsp;&nbsp;Account
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                        &nbsp;&nbsp;Message
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                        &nbsp;&nbsp;Help
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        &nbsp;&nbsp;Log Out 
                    </a>
                </li>
                <li>
                    <a href="#menu-close" id="menu-close">
                        <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                        <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                        <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <!--<a href="#menu-open" class="btn btn-default" id="menu-open"><span class="glyphicon glyphicon-resize-horizontal" aria-hidden="true"></span></a>-->
 
    <div id="preloader">
        <div id="status"></div>
    </div>
    
    <!-- FullScreen -->
    <div class="intro-header">
        <div class="col-xs-12 text-center abcen1">
            <h1 class="h1_home wow fadeIn" data-wow-delay="0.4s">Seahorse Ink <span class="glyphicon glyphicon-tint" aria-hidden="true"></span></h1>
            <h3 class="h3_home wow fadeIn" data-wow-delay="0.6s"><font color="black">Let's Socialize</font></h3>
            <ul class="list-inline intro-social-buttons">
                <li><a href="http://cgi.cse.unsw.edu.au/~z5076031/labs/lab12/project/Website.pl" class="btn  btn-lg mybutton_cyano wow fadeIn" data-wow-delay="0.8s"><span class="network-name">Projects</span></a>
                </li>
                <li id="download" ><a href="#downloadlink" class="btn  btn-lg mybutton_cyano wow fadeIn" data-wow-delay="1.2s"><span class="network-name">Facebook</span></a>
                </li>
            </ul>
        </div>    
        <!-- /.container -->
        <div class="col-xs-12 text-center abcen wow fadeIn">
            <div class="button_down "> 
                <a class="imgcircle wow bounceInUp" data-wow-duration="1.5s"  href="#whatis"> <img class="img_scroll" src="img/icon/circle.png" alt=""> </a>
            </div>
        </div>
    </div>
    
    <!-- What is -->
    <div id="whatis" class="content-section-b" style="border-top: 0">
        <div class="container">

            <div class="col-md-6 col-md-offset-3 text-center wrap_title">
                <h2>What is This?</h2>
                <p class="lead" style="margin-top:0">A website to socialize in</p>
                
            </div>
            
            <div class="row">
            
                <div class="col-sm-4 wow fadeInDown text-center">
                  <img class="rotate" src="img/icon/tweet.svg" alt="Generic placeholder image">
                  <h3>Messaging</h3>
                  <p class="lead">This site offers messageing services </p>

                  <!-- <p><a class="btn btn-embossed btn-primary view" role="button">View Details</a></p> -->
                </div><!-- /.col-lg-4 -->
                
                <div class="col-sm-4 wow fadeInDown text-center">
                  <img  class="rotate" src="img/icon/picture.svg" alt="Generic placeholder image">
                   <h3>Gallery</h3>
                   <p class="lead">This website allows you to freely share images </p>
                   <!-- <p><a class="btn btn-embossed btn-primary view" role="button">View Details</a></p> -->
                </div><!-- /.col-lg-4 -->
                
                <div class="col-sm-4 wow fadeInDown text-center">
                  <img  class="rotate" src="img/icon/retina.svg" alt="Generic placeholder image">
                   <h3>Retina</h3>
                    <p class="lead">This is a high quality site which took time to make and code </p>
                  <!-- <p><a class="btn btn-embossed btn-primary view" role="button">View Details</a></p> -->
                </div><!-- /.col-lg-4 -->
                
            </div><!-- /.row -->
                
            <div class="row tworow">
            
                <div class="col-sm-4  wow fadeInDown text-center">
                  <img class="rotate" src="img/icon/laptop.svg" alt="Generic placeholder image">
                  <h3>Responsive</h3>
                  <p class="lead">This site does not lag and certain elements react to mouse movements  </p>
                 <!-- <p><a class="btn btn-embossed btn-primary view" role="button">View Details</a></p> -->
                </div><!-- /.col-lg-4 -->
                
                <div class="col-sm-4 wow fadeInDown text-center">
                  <img  class="rotate" src="img/icon/map.svg" alt="Generic placeholder image">
                   <h3>Google</h3>
                   <p class="lead">this is somthing i wrote becuse im soopercool yo yeah look and cri bby </p>
                   <!-- <p><a class="btn btn-embossed btn-primary view" role="button">View Details</a></p> -->
                </div><!-- /.col-lg-4 -->
                
                <div class="col-sm-4 wow fadeInDown text-center">
                  <img  class="rotate" src="img/icon/browser.svg" alt="Generic placeholder image">
                   <h3>CoolSite</h3>
                 <p class="lead">yes built by me cause i'm soopercool uncle. </p>
                  <!-- <p><a class="btn btn-embossed btn-primary view" role="button">View Details</a></p> -->
                </div><!-- /.col-lg-4 -->
                
            </div><!-- /.row -->
        </div>
    </div>
    
    <!-- Use it -->
    <div id ="useit" class="content-section-a">

        <div class="container">
            
            <div class="row">
            
                <div class="col-sm-6 pull-right wow fadeInRightBig">
                    <img class="img-responsive " src="img/ipad.png" alt="">
                </div>
                
                <div class="col-sm-6 wow fadeInLeftBig"  data-animation-delay="200">   
                    <h3 class="section-heading">Full Responsive</h3>
                    <div class="sub-title lead3">Lorem ipsum dolor sit atmet sit dolor greand fdanrh<br> sdfs sit atmet sit dolor greand fdanrh sdfs</div>
                    <p class="lead">
                        In his igitur partibus duabus nihil erat, quod Zeno commuta rest gestiret. 
                        Sed virtutem ipsam inchoavit, nihil ampliusuma. Scien tiam pollicentur, 
                        uam non erat mirum sapientiae lorem cupido
                        patria esse cariorem. Quae qui non vident, nihilamane umquam magnum ac cognitione.
                    </p>

                     <p><a class="btn btn-embossed btn-primary" href="#" role="button">View Details</a> 
                     <a class="btn btn-embossed btn-info" href="#" role="button">Visit Website</a></p>
                </div>   
            </div>
        </div>
        <!-- /.container -->
    </div>

    <div class="content-section-b"> 
        
        <div class="container">
            <div class="row">
                <div class="col-sm-6 wow fadeInLeftBig">
                     <div id="owl-demo-1" class="owl-carousel">
                        <a href="img/iphone.png" class="image-link">
                            <div class="item">
                                <img  class="img-responsive img-rounded" src="img/iphone.png" alt="">
                            </div>
                        </a>
                        <a href="img/iphone.png" class="image-link">
                            <div class="item">
                                <img  class="img-responsive img-rounded" src="img/iphone.png" alt="">
                            </div>
                        </a>
                        <a href="img/iphone.png" class="image-link">
                            <div class="item">
                                <img  class="img-responsive img-rounded" src="img/iphone.png" alt="">
                            </div>
                        </a>
                    </div>       
                </div>
                
                <div class="col-sm-6 wow fadeInRightBig"  data-animation-delay="200">   
                    <h3 class="section-heading">Drag Gallery</h3>
                    <div class="sub-title lead3">Lorem ipsum dolor sit atmet sit dolor greand fdanrh<br> sdfs sit atmet sit dolor greand fdanrh sdfs</div>
                    <p class="lead">
                        In his igitur partibus duabus nihil erat, quod Zeno commuta rest gestiret. 
                        Sed virtutem ipsam inchoavit, nihil ampliusuma. Scien tiam pollicentur, 
                        uam non erat mirum sapientiae lorem cupido
                        patria esse cariorem. Quae qui non vident, nihilamane umquam magnum ac cognitione.
                    </p>

                     <p><a class="btn btn-embossed btn-primary" href="#" role="button">View Details</a> 
                     <a class="btn btn-embossed btn-info" href="#" role="button">Visit Website</a></p>
                </div>              
            </div>
        </div>
    </div>

    <div class="content-section-a">

        <div class="container">

             <div class="row">
             
                <div class="col-sm-6 pull-right wow fadeInRightBig">
                    <img class="img-responsive " src="img/doge.png" alt="">
                </div>
             
                <div class="col-sm-6 wow fadeInLeftBig"  data-animation-delay="200">   
                    <h3 class="section-heading">Font Awesome & Glyphicon</h3>
                    <p class="lead">A special thanks to Death to the Stock Photo for 
                    providing the photographs that you see in this template. 
                    </p>
                    
                    <ul class="descp lead2">
                        <li><i class="glyphicon glyphicon-signal"></i> Reliable and Secure Platform</li>
                        <li><i class="glyphicon glyphicon-refresh"></i> Everything is perfectly orgainized for future</li>
                        <li><i class="glyphicon glyphicon-headphones"></i> Attach large file easily</li>
                    </ul>
                </div>           
            </div>
        </div>

    </div>

    <!-- Screenshot -->
    <div id="screen" class="content-section-b">
        <div class="container">
          <div class="row" >
             <div class="col-md-6 col-md-offset-3 text-center wrap_title ">
                <h2>Screen App</h2>
                <p class="lead" style="margin-top:0">A special thanks to Death.</p>
             </div>
          </div>
            <div class="row wow bounceInUp" >
              <div id="owl-demo" class="owl-carousel">
                
                <a href="img/slide/1.png" class="image-link">
                    <div class="item">
                        <img  class="img-responsive img-rounded" src="img/slide/1.png" alt="Owl Image">
                    </div>
                </a>
                
               <a href="img/slide/2.png" class="image-link">
                    <div class="item">
                        <img  class="img-responsive img-rounded" src="img/slide/2.png" alt="Owl Image">
                    </div>
                </a>
                
                <a href="img/slide/3.png" class="image-link">
                    <div class="item">
                        <img  class="img-responsive img-rounded" src="img/slide/3.png" alt="Owl Image">
                    </div>
                </a>
                
                <a href="img/slide/1.png" class="image-link">
                    <div class="item">
                        <img  class="img-responsive img-rounded" src="img/slide/1.png" alt="Owl Image">
                    </div>
                </a>
                
               <a href="img/slide/2.png" class="image-link">
                    <div class="item">
                        <img  class="img-responsive img-rounded" src="img/slide/2.png" alt="Owl Image">
                    </div>
                </a>
                
                <a href="img/slide/3.png" class="image-link">
                    <div class="item">
                        <img  class="img-responsive img-rounded" src="img/slide/3.png" alt="Owl Image">
                    </div>
                </a>
              </div>       
          </div>
        </div>


    </div>
    
    <div  class="content-section-c ">
        <div class="container">
            <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center wrap_title white">
                <h2>Get Live Updates</h2>
                <p class="lead" style="margin-top:0">A special thanks to yo.</p>
             </div>
            <div class="mockup-content">
                    <div class="morph-button wow pulse morph-button-inflow morph-button-inflow-1">
                        <button type="button "><span>Subscribe to our Newsletter</span></button>
                        <div class="morph-content">
                            <div>
                                <div class="content-style-form content-style-form-4 ">
                                    <h2 class="morph-clone">Subscribe to our Newsletter</h2>
                                    <form>
                                        <p><label>Your Email Address</label><input type="text"/></p>
                                        <p><button>Subscribe me</button></p>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>  
    
    <!-- Credits -->
    <div id="credits" class="content-section-a">
        <div class="container">
            <div class="row">
            
            <div class="col-md-6 col-md-offset-3 text-center wrap_title">
                <h2>Credits</h2>
                <p class="lead" style="margin-top:0">I cool yo.</p>
             </div>
             
                <div class="col-sm-6  block wow bounceIn">
                    <div class="row">
                        <div class="col-md-4 box-icon rotate"> 
                            <i class="fa fa-desktop fa-4x "> </i> 
                        </div>
                        <div class="col-md-8 box-ct">
                            <h3> Bootstrap </h3>
                            <p> Lorem ipsum dolor sit ametconsectetur adipiscing elit. Suspendisse orci quam. </p>
                        </div>
                  </div>
              </div>
              <div class="col-sm-6 block wow bounceIn">
                    <div class="row">
                      <div class="col-md-4 box-icon rotate"> 
                        <i class="fa fa-picture-o fa-4x "> </i> 
                      </div>
                      <div class="col-md-8 box-ct">
                        <h3> Owl-Carousel </h3>
                        <p> Nullam mo  arcu ac molestie scelerisqu vulputate, molestie ligula gravida, tempus ipsum.</p> 
                      </div>
                    </div>
              </div>
          </div>
          
          <div class="row tworow">
                <div class="col-sm-6  block wow bounceIn">
                    <div class="row">
                        <div class="col-md-4 box-icon rotate"> 
                            <i class="fa fa-magic fa-4x "> </i> 
                        </div>
                        <div class="col-md-8 box-ct">
                            <h3> Codrops </h3>
                            <p> Lorem ipsum dolor sit ametconsectetur adipiscing elit. Suspendisse orci quam. </p>
                        </div>
                  </div>
              </div>
              <div class="col-sm-6 block wow bounceIn">
                    <div class="row">
                      <div class="col-md-4 box-icon rotate"> 
                        <i class="fa fa-heart fa-4x "> </i> 
                      </div>
                      <div class="col-md-8 box-ct">
                        <h3> Lorem Ipsum</h3>
                        <p> Nullam mo  arcu ac molestie scelerisqu vulputate, molestie ligula gravida, tempus ipsum.</p> 
                      </div>
                    </div>
              </div>
          </div>
        </div>
    </div>
    
    <!-- Banner Download -->
    <div id="downloadlink" class="banner">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center wrap_title">
                <h2>cool button</h2>
                <p class="lead" style="margin-top:0">wahhhhhh</p>
                <p><a class="btn btn-embossed btn-primary view" role="button">oops this is the button</a></p> 
             </div>
            </div>
        </div>
    </div>
    
    <!-- Contact -->
    <div id="contact" class="content-section-a">
        <div class="container">
            <div class="row">
            
            <div class="col-md-6 col-md-offset-3 text-center wrap_title">
                <h2>Contact Us</h2>
                <p class="lead" style="margin-top:0">A special thanks to me.</p>
            </div>
            
            <form role="form" action="" method="post" >
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="InputName">Your Name</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="InputName" id="InputName" placeholder="Enter Name" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="InputEmail">Your Email</label>
                        <div class="input-group">
                            <input type="email" class="form-control" id="InputEmail" name="InputEmail" placeholder="Enter Email" required  >
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="InputMessage">Message</label>
                        <div class="input-group">
                            <textarea name="InputMessage" id="InputMessage" class="form-control" rows="5" required></textarea>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>

                    <input type="submit" name="submit" id="submit" value="Submit" class="btn wow tada btn-embossed btn-primary pull-right">
                </div>
            </form>
            
            <hr class="featurette-divider hidden-lg">
                <div class="col-md-5 col-md-push-1 address">
                    <address>
                    <h3>Office Location</h3>
                    <p class="lead"><a href="https://www.google.com/maps/preview?ie=UTF-8&q=The+Pentagon&fb=1&gl=us&hq=1400+Defense+Pentagon+Washington,+DC+20301-1400&cid=12647181945379443503&ei=qmYfU4H8LoL2oATa0IHIBg&ved=0CKwBEPwSMAo&safe=on">The Pentagon<br>
                    Clasified</a><br>
                    Phone: supersecret<br>
                    Fax: cannotacces</p>
                    </address>

                    <h3>Social</h3>
                    <li class="social"> 
                    <a href="#"><i class="fa fa-facebook-square fa-size"> </i></a>
                    <a href="#"><i class="fa  fa-twitter-square fa-size"> </i> </a> 
                    <a href="#"><i class="fa fa-google-plus-square fa-size"> </i></a>
                    <a href="#"><i class="fa fa-flickr fa-size"> </i> </a>
                    </li>
                </div>
            </div>
        </div>
    </div>
    
    
    
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-7">
            <h3 class="footer-title">Follow Me!</h3>
            <p>Vuoi ricevere news su altri template?<br/>
              Visita Andrea Galanti.it e vedrai tutte le news riguardanti nuovi Theme!<br/>
              Go to: <a  href="http://andreagalanti.it" target="_blank">andreagalanti.it</a>
            </p>
            
            <!-- LICENSE -->
            <a rel="cc:attributionURL" href="http://www.andreagalanti.it/flatfy"
           property="dc:title">Seahorse Ink, </a> by
           <a rel="dc:creator" href="http://www.andreagalanti.it"
           property="cc:attributionName">A few people</a>
           Me yo cool
           <BR>the <a rel="license"
           href="http://creativecommons.org/licenses/by-nc/3.0/it/deed.it">work?
           yep.</a>.
           
       
          </div> <!-- /col-xs-7 -->

          <div class="col-md-5">
            <div class="footer-banner">
              <h3 class="footer-title">Seahorse Ink.</h3>
              <ul>
                <li>12 Column Grid Bootstrap</li>
                <li>Form Contact</li>
                <li>Drag Gallery</li>
                <li>Full Responsive</li>
                <li>Lorem Ipsum</li>
              </ul>
              Go to: <a href="http://andreagalanti.it/flatfy" target="_blank">thedranmaster.j@gmail.com</a>
            </div>
          </div>
        </div>
      </div>
    </footer>
        
        <!--<div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Simple Sidebar</h1>
                        <p>This is a template.<br>kek<br><br><br><br><br></p>
                    </div>
                </div>
            </div>
        </div>-->
        <!-- /#page-content-wrapper -->

    </div>

    <!-- JavaScript -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/script.js"></script>
    <!-- StikyMenu -->
    <script src="js/stickUp.min.js"></script>
    <script type="text/javascript">
      jQuery(function($) {
        $(document).ready( function() {
          $('.navbar-default').stickUp();
          
        });
      });
    
    </script>
    <!-- Smoothscroll -->
    <script type="text/javascript" src="js/jquery.corner.js"></script> 
    <script src="js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
    <script src="js/classie.js"></script>
    <script src="js/uiMorphingButton_inflow.js"></script>
    <!-- Magnific Popup core JS file -->
    <script src="js/jquery.magnific-popup.js"></script> 
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-open").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>

    <script>
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    </script>


""" #% (details, n + 1) 


#
# HTML placed at the top of every page
#
def page_header():
    return """Content-Type: text/html

"""


#
# HTML placed at the bottom of every page
# It includes all supplied parameter values as a HTML comment
# if global variable debug is set
#
def page_trailer(parameters):
    html = ""
    if debug:
        html += "".join("<!-- %s=%s -->\n" % (p, parameters.getvalue(p)) for p in parameters)
    html += "</body>\n</html>"
    return html

if __name__ == '__main__':
    debug = 1
    main()

